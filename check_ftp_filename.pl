#!/usr/bin/perl

use warnings;
use strict;
use Monitoring::Plugin;
use DateTime;
use DateTime::Format::Strptime;
use File::Basename;
use Net::FTP;

my $VERSION = '1.0.0';
my $PROGNAME = basename($0);
my $dateNowUTC = DateTime->now;
my @remote_files;
my $fileCount = 0;
my $license = "";

my $np = Monitoring::Plugin->new(
        shortname       => "ftp_filedate",
        usage           => "Usage: %s --site=<ftp.example.com> --path=<ftp/path/> --search=<pattern> --user=<username> --password=<password> --number=<expexted #files> [--verbose|--help]",
        version         => $VERSION,
        blurb           => "Used to list/match filenames on an ftp site against the search pattern.",
        url             => "https://gitlab.com/Jedimaster0/check_ftp_filedate",
        license         => $license,
        plugin          => "check_ftp_filedate",
        timeout         => 60,
);

$np->add_arg('site|S=s',        "-S, --site=STRING      \n   site.com/path : Used to retreive file list. (REQUIRED)", undef, 1);
$np->add_arg('user|u=s',        "-u, --user=STRING      \n   Username. (REQUIRED)", undef, 1);
$np->add_arg('password|p=s',    "-p, --password=STRING  \n   Password. (REQUIRED)", undef, 1);
$np->add_arg('path|P=s',        "-P, --path=STRING      \n   Path to files. Ex: path/ (a following '/' may be required)", '', 0);
$np->add_arg('search|s=s',      "-s, --search=STRING    \n   Search string or pattern.", '', 0);
$np->add_arg('number|n=s',      "-n, --number=STRING    \n   Number of files expected in response.", 1, 0);
$np->getopts;

if ($np->opts->verbose){
    print "#########################\n";
    print "site           : " . $np->opts->site . "\n";
    print "user           : " . $np->opts->user . "\n";
    print "password       : " . $np->opts->password . "\n";
    print "path           : " . $np->opts->path . "\n";
    print "search pattern : " . $np->opts->search . "\n";
    print "required #files: " . $np->opts->number . "\n";
    print "=========================\n";
}

my $ftp = Net::FTP->new($np->opts->site)
    or die $np->plugin_exit( CRITICAL, "Could not connect to " . $np->opts->site . ". Check the ftp server/path." );
    if ($np->opts->verbose){ print "Connection established to: " . $np->opts->site . "\n"; }

$ftp->login($np->opts->user, $np->opts->password)
    or die $np->plugin_exit( CRITICAL, "Could not login to " . $np->opts->site . " with user " . $np->opts->user . ". Check username and password." );
    if ($np->opts->verbose){ print "Login successful...\n"; }

if ($np->opts->path ne ''){
    $ftp->cwd($np->opts->path)
        or die $np->plugin_exit( CRITICAL, "Could not change to dir/path: " . $np->opts->path );
        if ($np->opts->verbose){ print "Successfully set directory: " . $ftp->pwd() . "\n"; }
}

if ($np->opts->verbose){ print "========= files =========\n"; }

@remote_files = $ftp->ls($np->opts->search);
foreach my $file (@remote_files) {
    if ($np->opts->verbose){
        print "$file\n";
    }
    $fileCount++;
}
$ftp->quit();

if ($np->opts->verbose){ print "#########################\n"; }

$np->add_perfdata(
    label => "files",
    value => $fileCount
);

if ( $fileCount >= $np->opts->number ){
    $np->plugin_exit( OK, "$fileCount files matching pattern have been found." );
} else {
    $np->plugin_exit( CRITICAL, "$fileCount files have been found. Expected " . $np->opts->number ." files." );
}